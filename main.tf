terraform {
  backend "http" {
  }
}

module "test_jumpbox" {
    source = "gitlab.com/lab-infrastructure/xoa-dynamic-instance/local"
    version = "0.0.2"

    vm_name_prefix = "jumpbox-"
    vm_name_suffix = ""
    vm_name_description = "jumpbox"
    vm_count = 1 
    username_admin = var.username_admin
    public_key_admin = var.public_key_admin
    username_ansible = var.username_ansible
    public_key_ansible = var.public_key_ansible
    xen_pool_name = "xcp-ng-pool-01"
    xen_template_name = "ubuntu-focal-20.04-cloudimg-20220124"
    xen_storage_name = "iscsi-vm-store"
    xen_network_name = "50-shopnet"
    #vm_disk_size_gb = ""
    #vm_memory_size_gb = ""
    #vm_cpu_count = ""
}

output "dynamic_hostnames" {
    value = module.test_jumpbox.instance_hostnames
}

output "dynamic_ipv4" {
    value = module.test_jumpbox.instance_ipv4_addresses
}